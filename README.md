# README #


## Solucion al reto Polymath Ventures ##


### Compilar ###

La solucion esta implementada en Java, el proyecto requiere el JDK 8 y Maven 3 para compilar. Con ambos instalados ejecutamos:

`mvn clean package`

Esto descaragara todas las dependencias del proyecto y compilará, generando un directorio nuevo target con un archivo llamado 

`Categories-1.0-SNAPSHOT-jar-with-dependencies.jar`

### Ejecutar ###

Una vez compilado el proyecto es posible ejecutar el archivo con:

`java -jar Categories-1.0-SNAPSHOT-jar-with-dependencies.jar --rebuild`

`java -jar Categories-1.0-SNAPSHOT-jar-with-dependencies.jar --info <category id>`

*Por comodidad el archivo puede ser renombrado sin problemas*

### Binarios Precompilados ###

En la seccion [ Downloads ](https://bitbucket.org/elvin_gonzalez/categories/downloads/Solucion.zip) se encuentra una version precompilada. Para ejecutarla:

#### En linux ####

`./categories --rebuild`

`./categories --info <category id>`

#### En windows ####

`categories.bat --rebuild`

`categories.bat --info <category id>`

#### O desde ambos ####

`java -jar categories.jar --rebuild`

`java -jar categories.jar --info <category id>`