/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.dto.response;

import java.util.Arrays;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Elvin
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CategoryArray {

    private Category[] Category;

    public Category[] getCategory() {
        return Category;
    }

    public void setCategory(Category[] Category) {
        this.Category = Category;
    }

    @Override
    public String toString() {
        return "CategoryArray{" + "Category=" + Arrays.toString(Category) + '}';
    }
}
