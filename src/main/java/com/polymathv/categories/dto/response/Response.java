/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Elvin
 */
@XmlRootElement(name = "GetCategoriesResponse", namespace = "urn:ebay:apis:eBLBaseComponents")
@XmlAccessorType(XmlAccessType.FIELD)
public class Response {

    private CategoryArray CategoryArray;

    private String Ack;

    public CategoryArray getCategoryArray() {
        return CategoryArray;
    }

    public void setCategoryArray(CategoryArray CategoryArray) {
        this.CategoryArray = CategoryArray;
    }

    public String getAck() {
        return Ack;
    }

    public void setAck(String Ack) {
        this.Ack = Ack;
    }

    @Override
    public String toString() {
        return "Response{" + "CategoryArray=" + CategoryArray + ", Ack=" + Ack + '}';
    }
}
