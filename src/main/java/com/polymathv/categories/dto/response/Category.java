/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Elvin
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Category {

    private boolean BestOfferEnabled;

    private String CategoryName;

    private String CategoryID;

    private String LSD;

    private int CategoryLevel;

    private String CategoryParentID;

    public boolean isBestOfferEnabled() {
        return BestOfferEnabled;
    }

    public void setBestOfferEnabled(boolean BestOfferEnabled) {
        this.BestOfferEnabled = BestOfferEnabled;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String CategoryID) {
        this.CategoryID = CategoryID;
    }

    public String getLSD() {
        return LSD;
    }

    public void setLSD(String LSD) {
        this.LSD = LSD;
    }

    public int getCategoryLevel() {
        return CategoryLevel;
    }

    public void setCategoryLevel(int CategoryLevel) {
        this.CategoryLevel = CategoryLevel;
    }

    public String getCategoryParentID() {
        return CategoryParentID;
    }

    public void setCategoryParentID(String CategoryParentID) {
        this.CategoryParentID = CategoryParentID;
    }

    @Override
    public String toString() {
        return "Category{" + "BestOfferEnabled=" + BestOfferEnabled + ", CategoryName=" + CategoryName + ", CategoryID=" + CategoryID + ", LSD=" + LSD + ", CategoryLevel=" + CategoryLevel + ", CategoryParentID=" + CategoryParentID + '}';
    }
}
