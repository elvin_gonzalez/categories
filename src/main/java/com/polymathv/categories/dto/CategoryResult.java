/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.dto;

import com.polymathv.categories.dto.response.Category;
import java.util.List;

/**
 *
 * @author usuario
 */
public class CategoryResult {

    private Category category;
    private List<Category> parents;

    public CategoryResult(Category category, List<Category> parents) {
        this.category = category;
        this.parents = parents;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Category> getParents() {
        return parents;
    }

    public void setParents(List<Category> parents) {
        this.parents = parents;
    }

    @Override
    public String toString() {
        return "CategoryResult{" + "category=" + category + ", parents=" + parents + '}';
    }

}
