/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.dto.request;

/**
 *
 * @author Elvin
 */
public class RequesterCredentials {
    private String eBayAuthToken;

    public String getEBayAuthToken ()
    {
        return eBayAuthToken;
    }

    public void setEBayAuthToken (String eBayAuthToken)
    {
        this.eBayAuthToken = eBayAuthToken;
    }

    @Override
    public String toString() {
        return "RequesterCredentials{" + "eBayAuthToken=" + eBayAuthToken + '}';
    }
}
