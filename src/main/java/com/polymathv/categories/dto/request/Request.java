/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.dto.request;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Elvin
 */
@XmlRootElement(name = "GetCategoriesRequest")
public class Request {
    private String CategoryParent;

    private String CategorySiteID;

    private String ViewAllNodes;

    private String xmlns;

    private String DetailLevel;

    private RequesterCredentials RequesterCredentials;

    public Request() {
        this.RequesterCredentials = new RequesterCredentials();
    }

    public String getCategoryParent ()
    {
        return CategoryParent;
    }

    public void setCategoryParent (String CategoryParent)
    {
        this.CategoryParent = CategoryParent;
    }

    public String getCategorySiteID ()
    {
        return CategorySiteID;
    }

    public void setCategorySiteID (String CategorySiteID)
    {
        this.CategorySiteID = CategorySiteID;
    }

    public String getViewAllNodes ()
    {
        return ViewAllNodes;
    }

    public void setViewAllNodes (String ViewAllNodes)
    {
        this.ViewAllNodes = ViewAllNodes;
    }

    public String getXmlns ()
    {
        return xmlns;
    }

    public void setXmlns (String xmlns)
    {
        this.xmlns = xmlns;
    }

    public String getDetailLevel ()
    {
        return DetailLevel;
    }

    public void setDetailLevel (String DetailLevel)
    {
        this.DetailLevel = DetailLevel;
    }

    public RequesterCredentials getRequesterCredentials ()
    {
        return RequesterCredentials;
    }

    public void setRequesterCredentials (RequesterCredentials RequesterCredentials)
    {
        this.RequesterCredentials = RequesterCredentials;
    }

    @Override
    public String toString() {
        return "Request{" + "CategoryParent=" + CategoryParent + ", CategorySiteID=" + CategorySiteID + ", ViewAllNodes=" + ViewAllNodes + ", xmlns=" + xmlns + ", DetailLevel=" + DetailLevel + ", RequesterCredentials=" + RequesterCredentials + '}';
    }
}
