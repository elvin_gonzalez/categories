/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.dto;

/**
 * Models the commands accepted by the application
 *
 * @author Elvin
 */
public class Command {

    private final boolean rebuild;
    private final boolean info;
    private final String categoryId;

    public Command() {
        rebuild = true;
        info = false;
        categoryId = null;
    }

    public Command(String categoryId) {
        rebuild = false;
        info = true;
        this.categoryId = categoryId;
    }
    
    public Command(boolean rebuild, boolean info, String categoryId) {
        this.rebuild = rebuild;
        this.info = info;
        this.categoryId = categoryId;
    }

    public boolean isRebuild() {
        return rebuild;
    }

    public boolean isInfo() {
        return info;
    }

    public String getCategoryId() {
        return categoryId;
    }

    @Override
    public String toString() {
        return "Command{" + "rebuild=" + rebuild + ", info=" + info + ", categoryId=" + categoryId + '}';
    }

}
