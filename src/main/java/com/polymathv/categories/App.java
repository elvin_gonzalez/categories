package com.polymathv.categories;

import com.polymathv.categories.dao.CategoriesDAO;
import com.polymathv.categories.dto.CategoryResult;
import com.polymathv.categories.dto.Command;
import com.polymathv.categories.exception.ArgumentException;
import com.polymathv.categories.exception.CategoriesServiceException;
import com.polymathv.categories.exception.NoDatabaseFoundException;
import com.polymathv.categories.renderer.ConsoleRenderer;
import com.polymathv.categories.renderer.Renderer;
import java.sql.SQLException;

public class App 
{
    public static final String REBUILD_COMMAND = "--rebuild";
    public static final String INFO_COMMAND = "--info";
    
    /**
     * Entry point of the application
     * @param args
     */
    public static void main( String[] args ) 
    {
        try {
            Command command = parseCommands(args);
            CategoriesDAO dao = new CategoriesDAO();
            if(command.isRebuild()){
                System.out.println("Rebuilding categories database");
                CategoriesFacade facade = new CategoriesFacade();
                dao.insertAll(facade.getAll());
                System.out.println("Database successfully rebuilt");
            }else if(command.isInfo()){
                CategoryResult result = dao.getCategoryWithParents(command.getCategoryId());
                if(result.getCategory() == null){
                    throw new ArgumentException("No category with ID: " + command.getCategoryId());
                }
                
                Renderer renderer = new ConsoleRenderer(result);
                System.out.println(renderer.render());
            }
        } catch (ArgumentException ex) {
            System.err.println(ex.getMessage());
        } catch (ClassNotFoundException | SQLException | CategoriesServiceException ex){
            System.err.println("A problem was found during execution. Please check the file and folder's permissions.");
        } catch (NoDatabaseFoundException ex) {
            System.err.println("No data found. Please run rebuild command first.");
        } catch (Throwable ex){
            System.err.println("An unexpected error ocurred");
            ex.printStackTrace();
        }
    }
    
    /**
     * Parse the application arguments and return a Command instance
     * @param args command line arguments
     * @return Command model instance
     * @throws ArgumentException in case no argument are received or are not valid.
     */
    private static Command parseCommands(String[] args) throws ArgumentException{
        if(args.length == 0){
            throw new ArgumentException("No command issued");
        }
        
        Command command = null;
        if(args[0].equalsIgnoreCase(INFO_COMMAND)){
            if(args.length < 2){
                throw new ArgumentException("No category id to render was suplied");
            }
            command = new Command(args[1]);
        }else if(args[0].equalsIgnoreCase(REBUILD_COMMAND)){
            command = new Command();
        }
        
        if(command == null){
            throw new ArgumentException("Invalid command");
        }
        
        return command;
    }
}
