/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.dao;

import com.polymathv.categories.dto.CategoryResult;
import com.polymathv.categories.dto.response.Category;
import com.polymathv.categories.exception.NoDatabaseFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages the database for the categories
 *
 * @author Elvin
 */
public class CategoriesDAO {

    private static final String CONNECTION_STRING = "jdbc:sqlite:categories.db";
    private static final String HIERARCHICAL_CATEGORY_QUERY = "WITH RECURSIVE"
            + "  category_tree(id,level) AS ("
            + "    VALUES(?,1)"
            + "    UNION ALL"
            + "    SELECT c.id, c.level"
            + "      FROM CATEGORIES c JOIN category_tree ON c.parentId=category_tree.id"
            + "     ORDER BY 2 DESC"
            + "  )"
            + "SELECT c.* FROM category_tree t JOIN CATEGORIES c ON C.id = t.id;";
    private static final String ALL_PARENTS_QUERY = "WITH RECURSIVE"
            + "   parent_category(id, name, parentId) as("
            + "     select id, name, parentId from categories where id = ?"
            + "     union"
            + "     select c.id, c.name, c.parentId from categories c, parent_category where c.id =parent_category.parentId"
            + "    )"
            + "select c.* from parent_category pc, categories c where c.id = pc.id;";

    /**
     * Default constructor
     *
     * @throws ClassNotFoundException
     */
    public CategoriesDAO() throws ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
    }

    /**
     * Fetch the whole tree of categories using the passed parentId as root. The
     * results are returned in hierarchical order.
     *
     * @param parentId
     * @return
     * @throws SQLException
     * @throws NoDatabaseFoundException
     */
    public List<Category> getHierarchyByParent(String parentId) throws SQLException, NoDatabaseFoundException {
        List<Category> categories = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(CONNECTION_STRING)) {
            if (!TableExists(connection)) {
                throw new NoDatabaseFoundException("No data found");
            }
            try (PreparedStatement selectStatement = connection.prepareStatement(HIERARCHICAL_CATEGORY_QUERY)) {
                selectStatement.setString(1, parentId);
                ResultSet result = selectStatement.executeQuery();
                while (result.next()) {
                    Category category = new Category();
                    category.setCategoryID(result.getString("id"));
                    category.setCategoryName(result.getString("name"));
                    category.setCategoryParentID(result.getString("parentId"));
                    category.setCategoryLevel(result.getInt("level"));
                    category.setBestOfferEnabled(result.getBoolean("bestOfferEnabled"));
                    categories.add(category);
                }
            }

        }
        return categories;
    }
    
    public CategoryResult getCategoryWithParents(String categoryId) throws SQLException, NoDatabaseFoundException{
        Category category = new Category();
        List<Category> parents = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(CONNECTION_STRING)) {
            if (!TableExists(connection)) {
                throw new NoDatabaseFoundException("No data found");
            }
            try (PreparedStatement selectStatement = connection.prepareStatement(ALL_PARENTS_QUERY)) {
                selectStatement.setString(1, categoryId);
                ResultSet result = selectStatement.executeQuery();
                if (result.next()) {
                    category.setCategoryID(result.getString("id"));
                    category.setCategoryName(result.getString("name"));
                    category.setCategoryParentID(result.getString("parentId"));
                    category.setCategoryLevel(result.getInt("level"));
                    category.setBestOfferEnabled(result.getBoolean("bestOfferEnabled"));
                }
                while(result.next()){
                    Category parent = new Category();
                    parent.setCategoryID(result.getString("id"));
                    parent.setCategoryName(result.getString("name"));
                    parent.setCategoryParentID(result.getString("parentId"));
                    parent.setCategoryLevel(result.getInt("level"));
                    parent.setBestOfferEnabled(result.getBoolean("bestOfferEnabled"));
                    parents.add(parent);
                }
            }

        }
        return new CategoryResult(category, parents);
    }

    /**
     * Verifies if the categories table exists so it can be safetly queried
     *
     * @param connection
     * @return
     * @throws SQLException
     */
    private boolean TableExists(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement();) {
            String tableExistsQuery = "SELECT name FROM sqlite_master WHERE type='table' AND name='CATEGORIES';";
            return statement.executeQuery(tableExistsQuery).next();
        }
    }

    /**
     * Insert all the categories into database as a batch in a single
     * transaction. The table is dropped and recreated before insertion.
     *
     * @param categories
     * @throws SQLException
     */
    public void insertAll(List<Category> categories) throws SQLException {
        try (Connection connection = DriverManager.getConnection(CONNECTION_STRING);
                Statement ddlStatement = connection.createStatement();) {

            ddlStatement.executeUpdate("DROP TABLE IF EXISTS CATEGORIES");
            ddlStatement.executeUpdate("CREATE TABLE CATEGORIES(id string PRIMARY KEY, name string, level integer, bestOfferEnabled boolean, parentId string REFERENCES CATEGORIES) WITHOUT ROWID");

            connection.setAutoCommit(false);
            try (PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO CATEGORIES VALUES (?,?,?,?,?)")) {
                for (final Category category : categories) {
                    insertStatement.setString(1, category.getCategoryID());
                    insertStatement.setString(2, category.getCategoryName());
                    insertStatement.setInt(3, category.getCategoryLevel());
                    insertStatement.setBoolean(4, category.isBestOfferEnabled());
                    if (!category.getCategoryID().equals(category.getCategoryParentID())) {
                        insertStatement.setString(5, category.getCategoryParentID());
                    } else {
                        insertStatement.setString(5, null);
                    }
                    insertStatement.addBatch();
                }
                insertStatement.executeBatch();
                connection.commit();
            }
        }
    }
}
