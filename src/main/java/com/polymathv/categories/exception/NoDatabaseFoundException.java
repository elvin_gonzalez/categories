/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.exception;

/**
 * 
 * @author Elvin
 */
public class NoDatabaseFoundException extends Exception {

    public NoDatabaseFoundException() {
    }

    public NoDatabaseFoundException(String message) {
        super(message);
    }

    public NoDatabaseFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoDatabaseFoundException(Throwable cause) {
        super(cause);
    }
}
