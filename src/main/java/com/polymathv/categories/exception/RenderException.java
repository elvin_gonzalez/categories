/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.exception;

/**
 *
 * @author usuario
 */
public class RenderException extends Exception {

    public RenderException() {
    }

    public RenderException(String string) {
        super(string);
    }

    public RenderException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public RenderException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
