/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.exception;

/**
 *
 * @author Elvin
 */
public class CategoriesServiceException extends RuntimeException {

    public CategoriesServiceException() {
    }

    public CategoriesServiceException(String message) {
        super(message);
    }

    public CategoriesServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public CategoriesServiceException(Throwable cause) {
        super(cause);
    }
    
}
