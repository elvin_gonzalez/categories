/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.renderer;

import com.polymathv.categories.dto.CategoryResult;
import com.polymathv.categories.dto.response.Category;
import com.polymathv.categories.exception.RenderException;
import java.util.Iterator;

/**
 *
 * @author usuario
 */
public class ConsoleRenderer implements Renderer {

    private final CategoryResult categoryResult;

    public ConsoleRenderer(CategoryResult categoryResult) {
        this.categoryResult = categoryResult;
    }

    @Override
    public String render() throws RenderException {
        Category c = categoryResult.getCategory();
        StringBuilder builder = new StringBuilder();
        builder.append("CategoryId: ").append(c.getCategoryID()).append(System.lineSeparator());
        builder.append("CategoryName: ").append(c.getCategoryName()).append(System.lineSeparator());
        builder.append("CategoryLevel: ").append(c.getCategoryLevel()).append(System.lineSeparator());
        builder.append("BestOfferEnabled: ").append(c.isBestOfferEnabled()).append(System.lineSeparator());
        builder.append("Ancestors: ");

        for (int i = 0; i < categoryResult.getParents().size(); i++) {
            Category p = categoryResult.getParents().get(i);
            builder.append(String.format("%s (%s)", p.getCategoryID(), p.getCategoryName()));
            if(i != categoryResult.getParents().size() -1){
                builder.append(", ");
            }
        }

        return builder.toString();
    }

}
