/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.renderer;

import com.polymathv.categories.exception.RenderException;

/**
 *
 * @author usuario
 */
public interface Renderer {
    public String render() throws RenderException;
}
