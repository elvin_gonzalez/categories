/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polymathv.categories.renderer;

import com.polymathv.categories.dto.response.Category;
import com.polymathv.categories.exception.RenderException;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import org.rendersnake.HtmlCanvas;

/**
 * Renders a list of Category into a string as HTML as a hierarchical tree using
 * unordered list (ul) and list element (li) tags. This implementation depends
 * on that list is already order as hierarchy to correctly render the tree.
 *
 * @author Elvin
 */
public class CategoriesHTMLRenderer implements Renderer {

    private final List<Category> categories;

    public CategoriesHTMLRenderer(List<Category> categories) {
        this.categories = categories;
    }

    /**
     * Render the HTML string
     *
     * @return
     * @throws com.polymathv.categories.exception.RenderException
     */
    @Override
    public String render() throws RenderException {
        String result;

        try {
            HtmlCanvas canvas = new HtmlCanvas();
            HtmlCanvas body = canvas.html().body();

            //A LIFO queue to hold all the list tags(ul and li) that need to be closed later
            Deque<String> tagsToClose = new ArrayDeque<>();
            int previousLevel = 0;
            for (Category category : categories) {
                int currentLevel = category.getCategoryLevel();

                if (currentLevel > previousLevel) {
                    //Opening a new list for a new level
                    body = body.ul().li().write(category.getCategoryName());
                    tagsToClose.push("</ul>");
                    tagsToClose.push("</li>");
                } else if (currentLevel == previousLevel) {
                    //Closes the previous element and opens a new one on the same level
                    body = body.close(tagsToClose.pop()).li().write(category.getCategoryName());
                    tagsToClose.push("</li>");
                } else {
                //Closes all the list and elements between both levels and
                    //then creates a new list for this level
                    for (int i = 0; i <= (previousLevel - currentLevel); i++) {
                        body = body.close(tagsToClose.pop()).close(tagsToClose.pop());
                    }
                    body = body.ul().li().write(category.getCategoryName());
                    tagsToClose.push("</ul>");
                    tagsToClose.push("</li>");
                }

                previousLevel = currentLevel;
            }

            //Closes all the remaining tags
            while (!tagsToClose.isEmpty()) {
                body.close(tagsToClose.pop());
            }
            result = body._body()._html().toHtml();
        } catch (IOException e) {
            throw new RenderException("An error ocurred while rendering the html result", e);
        }

        return result;
    }
}
